# Android docker

Contains Android specific docker images used by Zilla.

## Example `.gitlab-ci.yml` file

```yml
image: registry.gitlab.com/zla/android-docker:1.0
```

References:

1. https://gitlab.com/showcheap/android-ci
2. https://hub.docker.com/r/uber/android-build-environment/~/dockerfile/
